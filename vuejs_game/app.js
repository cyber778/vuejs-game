new Vue({
  el: '#app',
  data: {
    playerHealth: 10,
    monsterHealth: 10,
    specialAttacks: 2,
    gameRunning: false
  },
  methods: { 
    giveUp: function(){
      this.monsterHealth--;
      this.playerHealth--;
    },
    newGame: function(){
      this.gameRunning = !this.gameRunning;
    },
    attack: function(){
      this.monsterHealth--; 
      this.playerHealth -= 3;
    },
    specialAttack: function(){
      this.monsterHealth -= 3;
      this.playerHealth--;
    },
    heal: function(){
      this.playerHealth = 10;
      this.playerHealth -= 3;
    },
  }
});
